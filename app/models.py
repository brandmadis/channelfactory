from django.db import models

class Setup(models.Model):
    key = models.IntegerField(default=0)
    apikey = models.CharField(max_length=200)
    
    def __str__(self):
        return self.apikey
        