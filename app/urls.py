from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    path('getDistance/<start>/<finish>', views.getDistance, name='getDistance'),
    path('saveAPI/<key>', views.saveAPI, name='saveAPI'),
    path('removeAPIKEY', views.removeAPIKEY, name='removeAPIKEY'),
]