from django.shortcuts import render, redirect
from django.http import HttpResponse
from app.models import Setup
import requests
import math

def index(request):
    if Setup.objects.filter(key=1).count() == 0:
        apikey = ""
    else: 
        apikey = Setup.objects.get(key=1)
    context = {'apikey': apikey}
    return render(request, "app/index.html", context)
  
def saveAPI(request, key):
    if Setup.objects.filter(id=1).count() == 1:
        replaceKey = Setup.objects.get(id=1)
        replaceKey.apikey = key
        replaceKey.save()
    else:
        Setup.objects.create(key=1, apikey=key)
    return HttpResponse("Key Saved")
    
def getDistance(request, start, finish):
    apikey = str(Setup.objects.get(key=1))
    linkStart = "https://maps.googleapis.com/maps/api/geocode/json?address=" + start + "&key=" + apikey
    geoStart = requests.get(linkStart).json()
    startLat = geoStart['results'][0]['geometry']['location']['lat']
    startLng = geoStart['results'][0]['geometry']['location']['lng']
    linkFinish = "https://maps.googleapis.com/maps/api/geocode/json?address=" + finish + "&key=" + apikey
    geoFinish = requests.get(linkFinish).json()
    finishLat = geoFinish['results'][0]['geometry']['location']['lat']
    finishLng = geoFinish['results'][0]['geometry']['location']['lng']
    deltaLat = finishLat - startLat
    deltaLng = finishLng - startLng
    radian = 3.141592654/180
    startLatR = startLat * radian
    sinL1 = math.sin(startLat * radian)
    sinL2 = math.sin(finishLat * radian)
    cosL1 = math.cos(startLat * radian)
    cosL2 = math.cos(finishLat * radian)
    cosL1L2 = math.cos((startLng * radian) - (finishLng * radian))
    d = math.acos((sinL1 * sinL2) + (cosL1 * cosL2 * cosL1L2))
    miles = 3959 * d
    start_address = geoStart['results'][0]['formatted_address']
    finish_address = geoFinish['results'][0]['formatted_address']
    context = {
        'start_address': start_address,
        'finish_address': finish_address,
        'miles': miles,
    }
    return render(request, "app/data.html", context)
    
def removeAPIKEY(request):
    key = Setup.objects.get(key=1)
    key.delete()
    return redirect("/")